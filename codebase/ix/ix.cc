#include <iostream>
#include <cstring>
#include <string>

#include <cassert>

#include "ix.h"

PagedFileManager* IndexManager::_pfm = NULL;
IndexManager* IndexManager::_index_manager = NULL;
IndexManager* IX_ScanIterator::_ixm = NULL;

IndexManager* IndexManager::instance()
{
    if(!_index_manager)
        _index_manager = new IndexManager();

    return _index_manager;
}

IndexManager::IndexManager()
{
    // Initialize the internal PagedFileManager instance
    _pfm = PagedFileManager::instance();
}

IndexManager::~IndexManager()
{
}

RC IndexManager::createFile(const string &fileName)
{

    //create file
    if (_pfm->createFile(fileName.c_str()))
        return IX_CREATE_FAILED;

    FileHandle fh;
    if (_pfm->openFile(fileName, fh))
        return IX_CREATE_FAILED;

    void *pageSpace = calloc(PAGE_SIZE, 1);
    if (pageSpace == NULL)
        return IX_MALLOC_FAILED;

    // add new MetaPage
    MetaHeader mh;
    memcpy(pageSpace, &mh, sizeof(MetaHeader));
    if (fh.appendPage(pageSpace)) {
	free (pageSpace);
        return IX_CREATE_FAILED;
    }

    // add new Root 
    memset(pageSpace, 0, PAGE_SIZE);
    NodeHeader nh;
    nh.pageNum = 1;
    memcpy(pageSpace, &nh, sizeof(NodeHeader));
    if (fh.appendPage(pageSpace)) {
	free (pageSpace);
        return IX_CREATE_FAILED;
    }

    assert (fh.getNumberOfPages() == 2 && "Should have MetaPage and RootPage");
    free (pageSpace);
    return _pfm->closeFile(fh);
}

RC IndexManager::destroyFile(const string &fileName)
{
    return _pfm->destroyFile(fileName);
}


RC IndexManager::openFile(const string &fileName, IXFileHandle &ixfileHandle)
{
    if (_pfm->openFile(fileName, ixfileHandle._fh))
        return IX_OPEN_FAILED;

    if (ixfileHandle.getNumberOfPages() < 2) {
        return IX_OPEN_FAILED;
    }
    
    void* pageSpace = calloc(PAGE_SIZE, 1);
    if (pageSpace == NULL) return IX_MALLOC_FAILED;

    // read meta page 0 and get root pageNum
    if (ixfileHandle.readPage(0, pageSpace)) {
        free(pageSpace);
        return IX_OPEN_FAILED;
    }
    MetaHeader mh;
    memcpy(&mh, pageSpace, sizeof(MetaHeader));
    ixfileHandle._root = mh.root; 

    free(pageSpace);
    return SUCCESS;
}

RC IndexManager::closeFile(IXFileHandle &ixfileHandle)
{
    ixfileHandle._root = 0;
    return _pfm->closeFile(ixfileHandle._fh);
}

inline void IndexManager::getNodeHeader(const void* pageSpace, NodeHeader &nodeHeader) {
    memcpy(&nodeHeader, pageSpace, sizeof(NodeHeader)); 
}

inline void IndexManager::setNodeHeader(const NodeHeader &nodeHeader, void* &pageSpace) {
    memcpy(pageSpace, &nodeHeader, sizeof(NodeHeader)); 
}


inline void IndexManager::getEntrySlot(const void* pageSpace, EntryNum entryNum, EntrySlot &entrySlot) {
    // this funciton must not implement range check
    memcpy(&entrySlot, (char*)pageSpace + sizeof(NodeHeader) + entryNum * sizeof(EntrySlot), sizeof(EntrySlot));
}

inline void IndexManager::setEntrySlot(EntryNum entryNum, const EntrySlot &entrySlot, void* &pageSpace) {
    // this funciton must not implement range check
    memcpy((char*)pageSpace + sizeof(NodeHeader) + entryNum * sizeof(EntrySlot), &entrySlot, sizeof(EntrySlot));
}

inline bool IndexManager::isEqual(RID rid1, RID rid2) {
    return rid1.pageNum == rid2.pageNum && rid1.slotNum == rid2.slotNum;
}
int IndexManager::compareKeys(const Attribute &attribute, const void* key1, const void* key2) {
    cerr << ">>> compareKeys: (";
    printKey(attribute, key1);
    cerr << ", ";
    printKey(attribute, key2);
    cerr << endl;
     // get comparison result
    int result = 0;
    if (attribute.type == TypeReal) {
        float floatValue1 = 0.0;
        float floatValue2 = 0.0;
        memcpy (&floatValue1, key1, sizeof(float));
        memcpy (&floatValue2, key2, sizeof(float));
        if (floatValue1 < floatValue2) {
            result = -1;
        } else if (floatValue1 > floatValue2) {
            result = 1;
        }
    } else {
        int32_t intValue1 = 0; 
        int32_t intValue2 = 0;
        memcpy(&intValue1, key1, sizeof(int32_t));
        memcpy(&intValue2, key2, sizeof(int32_t));
        if (attribute.type == TypeInt) {
            result = intValue1 - intValue2;
        } else if (attribute.type == TypeVarChar) {
            char* varChar1 = (char*) calloc(intValue1 + 1, sizeof(char));
            if (varChar1 == NULL) {
                return IX_MALLOC_FAILED;
            }
            char* varChar2 = (char*) calloc(intValue2 + 1, sizeof(char));
            if (varChar1 == NULL) {
                free (varChar1);
                return IX_MALLOC_FAILED;
            }
            varChar1[intValue1] = '\0';                
            varChar2[intValue2] = '\0';                
            memcpy (varChar1, (char*)key1 + sizeof(int32_t), intValue1);
            memcpy (varChar2, (char*)key2 + sizeof(int32_t), intValue2);
            
            result = strcmp(varChar1, varChar2);
           
            free (varChar1);
            free (varChar2);
        } else {
            assert(false && "Error: Invalid attribute type\n");
        }
    }

    cerr << "<<< compareKeys: (";
    printKey(attribute, key1);
    cerr << ", ";
    printKey(attribute, key2);
    cerr << ") result = " << result << endl;
 
    return result;
}

// does not check if things are deleted so that they can still be found
// entries:  F [ GOOD] R [ GOOD] R [TIMES] R
// key1:       [ GOOD] ^
// key2:     ^ [ ALL ]
// key3:                         ^ [GREAT]
// key4:                                   ^ [TODAY]
// true if entry[entryNum]  == key    // child will be entry[entryNum].rightChild
// else is entry[entryNum-1] < key   // child will be entry[entryNum-1].rightChild
// rid is only changed if node is a leaf and return true
bool IndexManager::binarySearchNodeForKey(const void *pageSpace, const Attribute &attribute, const void *key, EntryNum &entryNum) {
    entryNum = 0;
    NodeHeader nodeHeader;
    getNodeHeader(pageSpace, nodeHeader);
    Offset keyOffset = (nodeHeader.nodeType == LEAF) ? sizeof(RID) : 0;
    
    int low = 0;
    int high = nodeHeader.entryCount - 1;
    EntrySlot entrySlot;
    while (low <= high) {
        int mid = (low + high) >> 1;
        getEntrySlot(pageSpace, mid, entrySlot);
        int result = compareKeys(attribute, key, (char*)pageSpace + entrySlot.offset + keyOffset);
        // process comparison result
        if (result < 0) {
            high = mid - 1;
        } else if (result > 0) {
            low = mid + 1;
        } else {
            entryNum = mid;
            return true; // exact match found
        }
    }//end while
    entryNum = low;
    return false; // exact match not found
}

// does not check for deletes so that deletes are still sorted
// PRE: rid is unique
// @return: entryNum = earliest insertPos
bool IndexManager::searchLeafForKeyInclusive(const void *pageSpace, const Attribute &attribute, const void *key, EntryNum &entryNum) {
    entryNum = 0;
    NodeHeader nodeHeader;
    getNodeHeader(pageSpace, nodeHeader);
    assert (nodeHeader.nodeType == LEAF && "BinarySearchOnLeafForKeyInclusive must be on a leaf node\n");
    if (!binarySearchNodeForKey(pageSpace, attribute, key, entryNum)) {
        return false;
    }
    EntrySlot entrySlot;
    EntryNum i = 0;
    // note: we already checked entryNum so
    // we do not have to check entryNum == 0 case
    if (entryNum > 0) {
        // search the entries of i < entryNum 
        for (i = entryNum - 1; i > 0; --i) {
            getEntrySlot(pageSpace, i, entrySlot);
            if (compareKeys(attribute, key, (char*)pageSpace + entrySlot.offset + sizeof(RID)) != 0) {
                // return the prior entry
                entryNum = i + 1;
                return false;
            }
        } 
        assert (i == 0);
        // we must check i == 0 case
        //if (i == 0) {
        getEntrySlot(pageSpace, 0, entrySlot);
        if (compareKeys(attribute, key, (char*)pageSpace + entrySlot.offset + sizeof(RID)) == 0) {
            entryNum = 0;
            return true;
        }
        entryNum = 1;
    }
    return true;
}

// does not check for deletes so that deletes are still sorted
void IndexManager::searchLeafForKeyExclusive(const void *pageSpace, const Attribute &attribute, const void *key, EntryNum &entryNum) {
    entryNum = 0;
    NodeHeader nodeHeader;
    getNodeHeader(pageSpace, nodeHeader);
    assert (nodeHeader.nodeType == LEAF && "searchLeafForKeyExclusive must be onleaf node\n");
    if (!binarySearchNodeForKey(pageSpace, attribute, key, entryNum)) {
        return;
    }
    // search the entries of i > entryNum 
    EntrySlot entrySlot;
    unsigned i = entryNum + 1; 
    for (; i < nodeHeader.entryCount; ++i) {
        getEntrySlot(pageSpace, i, entrySlot);
        if (compareKeys(attribute, key, (char*)pageSpace + entrySlot.offset + sizeof(RID)) != 0) {
            entryNum = i;
            return;
        }
    }
    // all equal and now past bounds
    entryNum = i;
}

// PRE: rid is unique
// @return: entryNum = earliest insertPos
// WARNING: should find lowest, check for bugs
bool IndexManager::searchLeafForKeyAndRid(const void *pageSpace, const Attribute &attribute, const void *key, const RID &rid, EntryNum &entryNum) {
    NodeHeader nodeHeader;
    EntrySlot entrySlot;
    getNodeHeader(pageSpace, nodeHeader);
    assert (nodeHeader.nodeType == LEAF && "BinarySearchOnLeafForKeyAndRid must be on a leaf node\n");
    
    if (!binarySearchNodeForKey(pageSpace, attribute, key, entryNum)) {
        return false;
    }
    RID entryRid; 
    getRid(pageSpace, entrySlot, entryRid);
    // key match exists
    if (isEqual(rid, entryRid)) {
        return true;
    }
    
    EntryNum i = 0;
    // search the entries of i > entryNum 
    for (i = entryNum + 1; i < nodeHeader.entryCount; ++i) {
        getEntrySlot(pageSpace, i, entrySlot);
        if (compareKeys(attribute, key, (char*)pageSpace + entrySlot.offset + sizeof(RID)) != 0) {
            // done with checking upper portion
            break;
        }
        memcpy(&entryRid, (char*)pageSpace + entrySlot.offset, sizeof(RID)); 
        if (isEqual(rid, entryRid)) {
            entryNum = i;
            return true;
        }
    }
    // note: we already checked entryNum so
    // we do not have to check entryNum == 0 case
    i = 0;
    if (entryNum > 0) {
        // search the entries of i < entryNum 
        for (i = entryNum - 1; i > 0; --i) {
            getEntrySlot(pageSpace, i, entrySlot);
            if (compareKeys(attribute, key, (char*)pageSpace + entrySlot.offset + sizeof(RID)) != 0) {
                // return the prior i as entryNum
                entryNum = i + 1;
                return false;
            }
            memcpy(&entryRid, (char*)pageSpace + entrySlot.offset, sizeof(RID)); 
            if (isEqual(rid, entryRid)) {
                // return current i as entryNum
                entryNum = i;
                return true;
            }
            // otherwise keep searching for equal key & rid
        } 
    }
    // assert entry 1 has equal key but unequal rid
    assert (i == 0);
    // we must check i == 0 case
    getEntrySlot(pageSpace, 0, entrySlot);
    if (compareKeys(attribute, key, (char*)pageSpace + entrySlot.offset + sizeof(RID)) != 0) {
        entryNum = 1;
        return false;
    }
    entryNum = 0;
    memcpy(&entryRid, (char*)pageSpace + entrySlot.offset, sizeof(RID)); 
    return isEqual(rid, entryRid);
}

void IndexManager::setEntrySlotLengthToKey(const Attribute &attribute, const void *key, EntrySlot &entrySlot) {
    entrySlot.length = sizeof(uint32_t);
    if (attribute.type == TypeVarChar) {
        uint32_t length = 0;
        memcpy(&length, key, sizeof(uint32_t));
        entrySlot.length += length;    
    }
}

// does an insert into specified entryNum position, no checks are performed
// updates count
void IndexManager::insertIntoPosition(const Attribute &attribute, const void* entry, const EntryNum &entryNum, EntrySlot &entrySlot, NodeHeader &nodeHeader, void* &pageSpace)  {

    // insert entry
    entrySlot.offset = nodeHeader.freeSpaceOffset - entrySlot.length;
    memcpy((char*)pageSpace + entrySlot.offset, entry, entrySlot.length);

    // insert entrySlot
    Offset src = sizeof(NodeHeader) + entryNum * sizeof(EntrySlot);
    Offset dst = src + sizeof(EntrySlot);
    memmove((char*)pageSpace + dst, (char*)pageSpace + src, (nodeHeader.entryCount - entryNum) * sizeof(EntrySlot));
    memcpy((char*)pageSpace + src, &entrySlot, sizeof(EntrySlot));

    // update nodeHeader
    nodeHeader.freeSpaceOffset = entrySlot.offset;
    ++nodeHeader.entryCount;
    nodeHeader.freeSpaceSize -= entrySlot.length + sizeof(EntrySlot);
    memcpy(pageSpace, &nodeHeader, sizeof(NodeHeader));
}

    
// does an insert into specified entryNum position, no checks are performed
// updates count
void IndexManager::insertIntoLeafPosition(const Attribute &attribute, const void* key, const RID &rid, const EntryNum &entryNum, EntrySlot &entrySlot, NodeHeader &nodeHeader, void* &pageSpace)  {
    // setup entrySlot.length
    setEntrySlotLengthToKey(attribute, key, entrySlot);
    entrySlot.length += sizeof(RID);

    // insert entry
    entrySlot.offset = nodeHeader.freeSpaceOffset - entrySlot.length;
    memcpy((char*)pageSpace + entrySlot.offset, &rid, sizeof(RID));
    memcpy((char*)pageSpace + entrySlot.offset + sizeof(RID), key, entrySlot.length - sizeof(RID));

    // insert entrySlot
    Offset src = sizeof(NodeHeader) + entryNum * sizeof(EntrySlot);
    Offset dst = src + sizeof(EntrySlot);
    memmove((char*)pageSpace + dst, (char*)pageSpace + src, (nodeHeader.entryCount - entryNum) * sizeof(EntrySlot));
    memcpy((char*)pageSpace + src, &entrySlot, sizeof(EntrySlot));

    // update nodeHeader
    nodeHeader.freeSpaceOffset = entrySlot.offset;
    ++nodeHeader.entryCount;
    nodeHeader.freeSpaceSize -= entrySlot.length + sizeof(EntrySlot);
    memcpy(pageSpace, &nodeHeader, sizeof(NodeHeader));
}

// PRE: MUST CHECK IF ENOUGH FREE SPACE prior
// POST: MUST WRITE CHANGES
// does an insert into specified entryNum position, no checks are performed
// updates count
void IndexManager::insertIntoInnerPosition(const Attribute &attribute, const void* key, const EntryNum &entryNum, EntrySlot &entrySlot, NodeHeader &nodeHeader, void* &pageSpace)  {
    cerr << ">>> insertIntoInnerPosition: <" << nodeHeader.pageNum << ","<<entryNum<<">:[";
    printKey(attribute, key);
    cerr << "]" << endl; 
 
    // setup entrySlot.length
    setEntrySlotLengthToKey(attribute, key, entrySlot);
    cerr << "entrySlot.length set to " << entrySlot.length << endl;

    // update entrySlot.offset
    entrySlot.offset = nodeHeader.freeSpaceOffset - entrySlot.length;
    cerr << "entrySlot.offset set to " << entrySlot.offset << endl;

    // insert entry
    memcpy((char*)pageSpace + entrySlot.offset, key, entrySlot.length);

    cerr << "entry \"";
    printKey(attribute, key); 
    cerr << " was inserted" << endl;
    
    
    // make gap for entrySlot
    Offset src = sizeof(NodeHeader) + entryNum * sizeof(EntrySlot);
    Offset dst = src + sizeof(EntrySlot);
    memmove((char*)pageSpace + dst, (char*)pageSpace + src, (nodeHeader.entryCount - entryNum) * sizeof(EntrySlot));

    // insert entrySlot into made gap
    memcpy((char*)pageSpace + src, &entrySlot, sizeof(EntrySlot));

    EntrySlot tempSlot;
    getEntrySlot(pageSpace, entryNum, tempSlot);
    cerr << "this is what we found: \"";
    printKey(attribute, (char*)pageSpace + tempSlot.offset); 
    cerr << "\"" << endl;

     // update nodeHeader
    nodeHeader.freeSpaceOffset = entrySlot.offset;
    ++nodeHeader.entryCount;
    nodeHeader.freeSpaceSize -= entrySlot.length + sizeof(EntrySlot);
    memcpy(pageSpace, &nodeHeader, sizeof(NodeHeader));

    // DELETE BELOW
    if (nodeHeader.nodeType == INNER) {
    cerr << "====== PAGE " << nodeHeader.pageNum << ": (AFTER INSERT) CURRENT KEYS =========" << endl;
                printInnerKeys(attribute, pageSpace, nodeHeader);
                cerr << endl << "====================================== " << endl;
            }
     


    cerr << "<<< insertIntoInnerPosition" << endl;// << nodeHeader.pageNum << ","<<entryNum<<">:[";
}

RC IndexManager::setMetaHeader(IXFileHandle &ixfh, MetaHeader &metaHeader) {
    RC rc = SUCCESS;
    void* pageSpace = (char*)calloc(PAGE_SIZE, 1);
    if (pageSpace == NULL) {
        return IX_MALLOC_FAILED;
    }
    rc = ixfh.readPage(0, pageSpace); 
    if (rc != SUCCESS) {
        free (pageSpace);
        return IX_READ_FAILED;
    }
    memcpy(pageSpace, &metaHeader, sizeof(metaHeader));
    free (pageSpace);
    return rc;
}

RC IndexManager::setRoot(IXFileHandle &ixfh, PageNum newRoot) {
    MetaHeader metaHeader;
    metaHeader.root = newRoot;
    ixfh.setRoot(newRoot);
    return setMetaHeader(ixfh, metaHeader);
}
    
RC IndexManager::splitRoot(IXFileHandle &ixfh, const Attribute &attribute, const PageNum leftChild, EntrySlot &entrySlot, void* &key) {
    cerr << ">>> splitRoot: key = ";
    printKey(attribute, key);
    cerr << endl;
    RC rc = SUCCESS;
    void* pageSpace = (char*)calloc(PAGE_SIZE, 1);
    if (pageSpace == NULL) {
        return IX_MALLOC_FAILED;
    }
    // create new root page
    NodeHeader nodeHeader;
    nodeHeader.pageNum = ixfh.getNumberOfPages();
    nodeHeader.nodeType = INNER;
    nodeHeader.firstChild = leftChild; 
    insertIntoInnerPosition(attribute, key, 0, entrySlot, nodeHeader, pageSpace);
    setNodeHeader(nodeHeader, pageSpace);
    rc = ixfh.appendPage(pageSpace); 
    free (pageSpace); 
    free (key);
    key = NULL;
    // update metaHeader
    setRoot(ixfh, nodeHeader.pageNum); 
    cerr << "<<< splitRoot: newRoot = " << nodeHeader.pageNum << endl;
    cerr << "               key = ";
    printKey(attribute, key);
    cerr << endl;
    return rc;
}

void IndexManager::copy(const Attribute &attribute, const NodeHeader &nodeHeader, const void* pageSpace, unsigned start, unsigned end, NodeHeader &newNodeHeader, void* &newPageSpace) {
    EntrySlot currEntrySlot;
    for (unsigned i = start; i < end; ++i) {
        getEntrySlot(pageSpace, i, currEntrySlot); 
        insertIntoPosition(attribute, (char*)pageSpace + currEntrySlot.offset, newNodeHeader.entryCount, currEntrySlot, newNodeHeader, newPageSpace);
    }
} 

// =========== LEAF SPLIT ===========================//
// PRE: check duplicate and find insert postion entryNum
RC IndexManager::splitInsertIntoLeafPosition(IXFileHandle &ixfh, const Attribute &attribute, const void* key, const RID &rid, EntryNum entryNum, EntrySlot &toParentEntrySlot, void* &toParentEntry, NodeHeader &nodeHeader, void* &pageSpace) {
    RC rc = SUCCESS;
    cerr << ">>> splitInsertIntoLeafPosition: <"<<nodeHeader.pageNum <<","<<entryNum<<">:[";
    printKey(attribute, key);
    cerr << ",";
    printRid(rid);
    cerr << "]" << endl;
    assert (nodeHeader.entryCount != 0 && "cannot split an empty node");
    // setup a new pageSpace
    void* newPageSpace = (char*)calloc(PAGE_SIZE, 1);
    if (newPageSpace == NULL) {
        return IX_MALLOC_FAILED;
    }
    memset (newPageSpace, 0, PAGE_SIZE);

    // set newNodeHeader and reorganize siblings
    NodeHeader newNodeHeader;
    NodeHeader newNodeHeader1;

    newNodeHeader.pageNum = ixfh.getNumberOfPages();
    newNodeHeader.nodeType = nodeHeader.nodeType;
    newNodeHeader.leftSibling = nodeHeader.pageNum;
    newNodeHeader.rightSibling = nodeHeader.rightSibling;
    nodeHeader.rightSibling = newNodeHeader.pageNum;
    newNodeHeader1.pageNum = nodeHeader.pageNum;
    newNodeHeader1.nodeType  = nodeHeader.nodeType; 
    newNodeHeader1.rightSibling = nodeHeader.rightSibling;

    // set newNodeRightSibling to point to newNode
    if (newNodeHeader.rightSibling != 0) {
        NodeHeader rightSibNodeHeader;
        rc = ixfh.readPage(newNodeHeader.rightSibling, newPageSpace);
        if (rc != SUCCESS) {
            free (newPageSpace);
            return IX_READ_FAILED;
        }
        getNodeHeader(newPageSpace, rightSibNodeHeader); 
        rightSibNodeHeader.leftSibling = newNodeHeader.pageNum;
        setNodeHeader(rightSibNodeHeader, newPageSpace);
        rc = ixfh.writePage(rightSibNodeHeader.pageNum, newPageSpace);
        if (rc != SUCCESS) {
            free (newPageSpace);
            return IX_WRITE_FAILED;
        }
    } 
    memset(newPageSpace, 0, PAGE_SIZE);
    assert(newNodeHeader.entryCount == 0);

    // calculate mid
    // [0][1] [[2]][3]
    // [0][1] [[2]][3][4]
    EntryNum mid = nodeHeader.entryCount >> 1;
    EntrySlot entrySlot;
    EntrySlot currEntrySlot;
    int result = 0;

    // compare entry with mid
    getEntrySlot(pageSpace, mid, entrySlot);
    result = compareKeys(attribute, key, (char*)pageSpace + entrySlot.offset);

    // check if split on same key 
    if (result == 0) {
        free (newPageSpace);
        return IX_SPLIT_SAME_KEY;
    }
    if (result > 0) {
        ++mid;
        entryNum -= mid;
    }
    
    // prepare entrySlot
    setEntrySlotLengthToKey(attribute, key, entrySlot);
    entrySlot.length += sizeof(RID);

    // A. Second Half: mid to entryCount
    copy (attribute, nodeHeader, pageSpace, mid, nodeHeader.entryCount, newNodeHeader, newPageSpace); 
    if (result > 0) {
        // insert into Second Half
        insertIntoLeafPosition(attribute, key, rid, entryNum, entrySlot, newNodeHeader, newPageSpace);
    }
    // Append changes
    rc = ixfh.appendPage(newPageSpace);
    if (rc != SUCCESS) {
        free (newPageSpace);
        return IX_APPEND_FAILED;
    }

    // B. Set toParentEntry
    getEntrySlot(newPageSpace, 0, toParentEntrySlot);
    toParentEntrySlot.length -= sizeof(RID);
    toParentEntry = (char*)calloc(toParentEntrySlot.length, 1);
    memcpy(toParentEntry, (char*)newPageSpace + toParentEntrySlot.offset + sizeof(RID), toParentEntrySlot.length);
    toParentEntrySlot.rightChild = newNodeHeader.pageNum;

 
    // C. First Half: 0 to mid
    memset(newPageSpace, 0, PAGE_SIZE);
    copy (attribute, nodeHeader, pageSpace, 0, mid, newNodeHeader1, newPageSpace); 
    if (result < 0) {
        // insert into First Half
        insertIntoLeafPosition(attribute, key, rid, entryNum, entrySlot, newNodeHeader1, newPageSpace);
    } 
   // Write changes to original node (First Half)
    setNodeHeader(newNodeHeader1, newPageSpace); // probably redundant
    getNodeHeader(newPageSpace, nodeHeader);
    memcpy(pageSpace, newPageSpace, PAGE_SIZE);
    rc = ixfh.writePage(nodeHeader.pageNum, pageSpace);
    if (rc != SUCCESS) {
        free (newPageSpace);
        return IX_WRITE_FAILED;
    }
 
   cerr << "before split check: toParentEntry = ";
    printKey(attribute, toParentEntry);
    cerr << endl;
    // D. Check if root
    if (nodeHeader.pageNum == ixfh.getRoot()) {
        splitRoot(ixfh, attribute, nodeHeader.pageNum, toParentEntrySlot, toParentEntry);
        cerr << "=== after splitRoot: toParentEntry = ";
        printKey(attribute, toParentEntry);
        cerr << endl;
    }

    free (newPageSpace);
    cerr << "<< splitInsertIntoLeafPosition: toParentEntry = ";
    printKey(attribute, toParentEntry);
    cerr << endl;
    cerr << "                                newNodeHeader.pageNum = " << newNodeHeader.pageNum << endl;
    cerr << "                                toParentEntrySlot.rightChild = " << toParentEntrySlot.rightChild << endl;
    return SUCCESS;
}

// =========== INNER SPLIT ===========================//

// PRE: check duplicate and find insert postion entryNum
RC IndexManager::splitInsertIntoInnerPosition(IXFileHandle &ixfh, const Attribute &attribute, EntryNum entryNum, EntrySlot &fromToParentEntrySlot, void* &fromToParentEntry, NodeHeader &nodeHeader, void* &pageSpace) {
    cerr << ">>> splitInsertIntoInnerPosition: <"<<nodeHeader.pageNum <<","<<entryNum<<">:[";
    printKey(attribute, fromToParentEntry);
    cerr << "];";
    cerr << ">>> splitInsertIntoInnerPosition" << endl;
    RC rc;
    assert (nodeHeader.entryCount != 0 && "cannot split an empty node");
    assert (fromToParentEntry != NULL && "must have key for split");

    // setup a new pageSpace
    void* newPageSpace = (char*)calloc(PAGE_SIZE, 1);
    if (newPageSpace == NULL) {
        return IX_MALLOC_FAILED;
    }
    memset (newPageSpace, 0, PAGE_SIZE);

    // set newNodeHeader
    NodeHeader newNodeHeader;
    NodeHeader newNodeHeader1;
    newNodeHeader.pageNum = ixfh.getNumberOfPages();
    newNodeHeader.nodeType = nodeHeader.nodeType;
    assert(newNodeHeader.entryCount == 0);

    newNodeHeader1.pageNum = nodeHeader.pageNum;
    newNodeHeader1.nodeType  = nodeHeader.nodeType; 

    // calculate mid
    EntryNum mid = nodeHeader.entryCount >> 1;
    EntrySlot entrySlot;
    EntrySlot currEntrySlot;
    int result = 0;
    if (mid == 0) {
        cerr << "Error: Key is too big" << endl;
        free (newPageSpace);
        return IX_LARGE_KEY;
    }
  
    // compare entry with mid
    getEntrySlot(pageSpace, mid, entrySlot);
    result = compareKeys(attribute, fromToParentEntry, (char*)pageSpace + entrySlot.offset);

    // if entry < mid
    if (result < 0) {
        // A. Second Half: from mid to entryCount    
        for (unsigned i = mid; i < nodeHeader.entryCount; ++i) {
            getEntrySlot(pageSpace, i, currEntrySlot); 
            insertIntoInnerPosition(attribute, (char*)pageSpace + currEntrySlot.offset, newNodeHeader.entryCount, currEntrySlot, newNodeHeader, newPageSpace);
        }
        rc = ixfh.appendPage(newPageSpace);
        if (rc != SUCCESS) {
            free (newPageSpace);
            free (fromToParentEntry);
            return IX_APPEND_FAILED;
        }

        // compare entry with mid - 1
        getEntrySlot(pageSpace, mid - 1, entrySlot);
        result = compareKeys(attribute, fromToParentEntry, (char*)pageSpace + entrySlot.offset);

        // check for a split on same value
        if (result == 0) {
            free (newPageSpace);
            free (fromToParentEntry);
            fromToParentEntry = NULL;
            return IX_SPLIT_SAME_KEY;
        }

        memset(newPageSpace, 0, PAGE_SIZE);
        // if entry < mid - 1
        if (result < 0) {
          // B. First Half:
          // from 0 to entryNum 
          copy(attribute, nodeHeader, pageSpace, 0, entryNum, newNodeHeader1, newPageSpace);
           // insert fromToParent entry into first half 
           insertIntoInnerPosition(attribute, fromToParentEntry, newNodeHeader1.entryCount, fromToParentEntrySlot, newNodeHeader1, newPageSpace);
           // from entryNum to mid - 1 
           copy(attribute, nodeHeader, pageSpace, entryNum, mid - 1, newNodeHeader1, newPageSpace);
           
           // C. PushUp: mid - 1
           // set fromToParentEntrySlot and fromToParentEntry
           if (fromToParentEntry != NULL) {
               free (fromToParentEntry);
           }
           getEntrySlot(pageSpace, mid - 1, fromToParentEntrySlot);
           fromToParentEntry = (char*)calloc(fromToParentEntrySlot.length, 1);
           memcpy(fromToParentEntry, (char*)pageSpace + fromToParentEntrySlot.offset, fromToParentEntrySlot.length);
           fromToParentEntrySlot.rightChild = newNodeHeader.pageNum;
        } else {
          // B. First Half: from 0 to mid 
          copy(attribute, nodeHeader, pageSpace, 0, mid, newNodeHeader1, newPageSpace);
          // C. PushUp: same FromToParent
        }
    } else { // entry >= mid
        // A. Second Half
        // from mid + 1 to entryNum
        unsigned i = mid + 1;
        for (; i < entryNum; ++i) {
            getEntrySlot(pageSpace, i, currEntrySlot); 
            insertIntoInnerPosition(attribute, (char*)pageSpace + currEntrySlot.offset, newNodeHeader.entryCount, currEntrySlot, newNodeHeader, newPageSpace);
        }
        // insert entry into second half  
        insertIntoInnerPosition(attribute, fromToParentEntry, newNodeHeader.entryCount, fromToParentEntrySlot, newNodeHeader, newPageSpace);
        // rest of second half
        for (; i < nodeHeader.entryCount; ++i) {
            getEntrySlot(pageSpace, i, currEntrySlot); 
            insertIntoInnerPosition(attribute, (char*)pageSpace + currEntrySlot.offset, newNodeHeader.entryCount, currEntrySlot, newNodeHeader, newPageSpace);
        }

        // B. First Half: from 0 to mid
        memset(newPageSpace, 0, PAGE_SIZE);
        copy(attribute, nodeHeader, pageSpace, 0, mid, newNodeHeader1, newPageSpace);
        
        // C. PushUp: mid

        // set fromToParentEntrySlot and fromToParentEntry
        if (fromToParentEntry != NULL) {
            free (fromToParentEntry);
        }
        getEntrySlot(pageSpace, mid, fromToParentEntrySlot);
        fromToParentEntry = (char*)calloc(fromToParentEntrySlot.length, 1);
        memcpy(fromToParentEntry, (char*)pageSpace + fromToParentEntrySlot.offset, fromToParentEntrySlot.length);
        fromToParentEntrySlot.rightChild = newNodeHeader.pageNum;

    }// end else
    
    // D. Write changes to original node (First Half)
    setNodeHeader(newNodeHeader1, newPageSpace); // probably redundant
    getNodeHeader(newPageSpace, nodeHeader);
    memset(pageSpace, 0, PAGE_SIZE);
    memcpy(pageSpace, newPageSpace, PAGE_SIZE);
    rc = ixfh.writePage(nodeHeader.pageNum, pageSpace);
    if (rc != SUCCESS) {
        free (newPageSpace);
        return IX_WRITE_FAILED;
    }
 
    cerr << "before split check: fromToParentEntry = ";
    printKey(attribute, fromToParentEntry);
    cerr << endl;
    // E. Check if root was split
    if (nodeHeader.pageNum == ixfh.getRoot()) {
        splitRoot(ixfh, attribute, nodeHeader.pageNum, fromToParentEntrySlot, fromToParentEntry);
    }

    free (newPageSpace);
    cerr << "<< splitInsertIntoInnerPosition" << endl;
    return SUCCESS;
}

// PRE: empty entrySlot
// PRE: newChildEntry = NULL
RC IndexManager::insertEntry2 (IXFileHandle &ixfh, const Attribute &attribute, const void *key, const RID &rid, PageNum node, EntrySlot &newChildEntrySlot, void* &newChildEntry) {
    RC rc = SUCCESS;
    // read in the node
    void* pageSpace = (char*)calloc(PAGE_SIZE, 1); 
    if (pageSpace == NULL) {
        if (newChildEntry != NULL) {
            free(newChildEntry);
            newChildEntry = NULL;
        }
        return IX_MALLOC_FAILED;
    }
    if ( ixfh.readPage(node, pageSpace) ) {
        if (newChildEntry != NULL) {
            free(newChildEntry);
            newChildEntry = NULL;
        }
        free (pageSpace);
        return IX_READ_FAILED;
    }
    
    NodeHeader nodeHeader;
    EntryNum entryNum = 0; 
    EntrySlot entrySlot;
    PageNum nextNode = 0;

    // read in the nodeHeader
    getNodeHeader(pageSpace, nodeHeader);
    
    if (nodeHeader.nodeType == INNER) {
        assert(nodeHeader.entryCount > 0 && "Inner Node cannot be empty\n");
        // 1. find next node
        if (binarySearchNodeForKey(pageSpace, attribute, key, entryNum) == true) {
            // nextNode == entryNum's rightChild
            getEntrySlot(pageSpace, entryNum, entrySlot);
            nextNode = entrySlot.rightChild; 
        } else {
            // nextNode == entryNum's leftChild
            if (entryNum == 0) {
                nextNode = nodeHeader.firstChild;
            } else {
                getEntrySlot(pageSpace, entryNum - 1, entrySlot);
                nextNode = entrySlot.rightChild; 
            }
        }
     
        // 2. insert into next node
        insertEntry2(ixfh, attribute, key, rid, nextNode, newChildEntrySlot, newChildEntry);
        // 
        if (rc != SUCCESS) {
            if (newChildEntry != NULL) {
                free(newChildEntry);
                newChildEntry = NULL;
            }
            free (pageSpace);
            return rc;
        } 

       // 3. check if child was split 
        if (newChildEntry == NULL) {
            free (pageSpace);
            return SUCCESS;
        }

        cerr << "Page Received: <" << nodeHeader.pageNum << ","<<entryNum<<">:[";
        printKey(attribute, newChildEntry);
        cerr << "]" << endl; 
        cerr << "Child was split! "<< endl;
        // 4. insert new entry from split into current page if room
        if (sizeof(EntrySlot) + newChildEntrySlot.length < nodeHeader.freeSpaceSize) {
            // DELETE BELOW
            if (nodeHeader.nodeType == INNER) {
                cerr << "====== PAGE " << nodeHeader.pageNum << ": CURRENT KEYS =========" << endl;
                printInnerKeys(attribute, pageSpace, nodeHeader);
                cerr << endl << "====================================== " << endl;
            }
            // DELETE ABOVE 

            cerr << "inserting into currpage: <" << nodeHeader.pageNum << ","<<entryNum<<">:[";
            printKey(attribute, newChildEntry);
            cerr << "]" << endl; 
           
            binarySearchNodeForKey(pageSpace, attribute, newChildEntry, entryNum);
            cerr << "newChildEntry's new entryNum is: " << entryNum << endl;
            
            insertIntoInnerPosition(attribute, newChildEntry, entryNum, newChildEntrySlot, nodeHeader, pageSpace);
            // update node
            rc = ixfh.writePage(nodeHeader.pageNum, pageSpace);
            free(newChildEntry);
            // set newChildEntry to NULL to stop further splits
            newChildEntry = NULL;
            free (pageSpace);
            return rc;
        }

        // 5. else split this page and return split result 
        cerr << "splitting currpage: <" << nodeHeader.pageNum << ","<<entryNum<<">:[";
        printKey(attribute, newChildEntry);
        cerr << endl; 
        rc = splitInsertIntoInnerPosition(ixfh, attribute, entryNum, newChildEntrySlot, newChildEntry, nodeHeader, pageSpace);
        if (rc != SUCCESS) {
            if (newChildEntry != NULL) {
                free(newChildEntry);
                newChildEntry = NULL;
            }
        }
        



        free (pageSpace);
        return rc;
    } // end of INNER NODE handling
    // LEAF node
    assert(nodeHeader.nodeType == LEAF);

    // 1. find insertion point
    if (searchLeafForKeyAndRid(pageSpace, attribute, key, rid, entryNum) == true) {
        getEntrySlot(pageSpace, entryNum, entrySlot);
        // just undelete if entry was deleted
        if (entrySlot.isDeleted == true) {
            entrySlot.isDeleted = false;
            free (pageSpace);
            return SUCCESS;
        }
        // else throw duplicate insert error
        free (pageSpace);
        return IX_DUP_INSERT;
    }

    // clear newChildEntrySlot 
    newChildEntrySlot.length = 0;
    newChildEntrySlot.offset = 0;
    newChildEntrySlot.rightChild = 0;
    newChildEntrySlot.isDeleted = false;

    // set newChildEntrySlot.length
    setEntrySlotLengthToKey(attribute, key, entrySlot);
    newChildEntrySlot.length += sizeof(RID);

    // 2. insert into found insertion point if room
    if (sizeof(EntrySlot) + newChildEntrySlot.length < nodeHeader.freeSpaceSize) {
        insertIntoLeafPosition(attribute, key, rid, entryNum, newChildEntrySlot, nodeHeader, pageSpace);
        // write changes 
        rc = ixfh.writePage(nodeHeader.pageNum, pageSpace);
        assert(newChildEntry == NULL && "PRE for insert: newChildEntry set to NULL");
        free (pageSpace);
        return rc;
    }

    // 3. else split this page and set newChildEntrySlot and newChildEntry
    rc = splitInsertIntoLeafPosition(ixfh, attribute, key, rid, entryNum, newChildEntrySlot, newChildEntry, nodeHeader, pageSpace);
    if (rc != SUCCESS) {
        if (newChildEntry != NULL) {
            free(newChildEntry);
            newChildEntry = NULL;
        }
    } 
    free (pageSpace);
    return rc; 
}

// PRE: ixfh is open and the root ixfh._root is up to date
// PRE: attribute is correct
RC IndexManager::insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid)
{
    cerr << "<< insertEntry: root "<< ixfileHandle.getRoot() << ": [";
    printKey(attribute, key);
    cerr << ",";
    printRid(rid);
    cerr << "]" << endl;
    RC rc = SUCCESS;
    // insert entry 
    EntrySlot entrySlot;
    void* newChildEntry = NULL;
    rc = insertEntry2 (ixfileHandle, attribute, key, rid, ixfileHandle._root, entrySlot, newChildEntry); 
    return rc;
   
}

RC IndexManager::goToLeaf(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, EntryNum &entryNum, NodeHeader &nodeHeader, void* &pageSpace)
{
     RC rc = ixfileHandle.readPage (ixfileHandle._root, pageSpace);
     if (rc != SUCCESS) {
         free (pageSpace);
         return IX_READ_FAILED;
     }
     getNodeHeader(pageSpace, nodeHeader);
     PageNum child = 0;
     EntrySlot entrySlot;
     while (nodeHeader.nodeType == INNER) {
        // find child  
        // false if value <  entry[entryNum]   // child will be entry[entryNum-1].rightChild
        // true  if value == entry[entryNum]   // child will be entry[entryNum].rightChild
        if (key == NULL) {
	    child = nodeHeader.firstChild;
	} else if (binarySearchNodeForKey(pageSpace, attribute, key, entryNum) == true) {
            getEntrySlot(pageSpace, entryNum, entrySlot); 
            child = entrySlot.rightChild;
        } else if (entryNum == 0) {
            child = nodeHeader.firstChild;
        } else { 
            getEntrySlot(pageSpace, entryNum - 1, entrySlot); 
            child = entrySlot.rightChild;
        }
        // go to child node
        rc = ixfileHandle.readPage (child, pageSpace);
        if (rc != SUCCESS) {
            free (pageSpace);
            return IX_READ_FAILED;
        }
        getNodeHeader(pageSpace, nodeHeader);
    } 
    return SUCCESS;
}

RC IndexManager::deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid)
{
    RC rc = SUCCESS;
    void* pageSpace = (char*) calloc (PAGE_SIZE, 1); 
    if (pageSpace == NULL) {
        return IX_MALLOC_FAILED;
    }
    NodeHeader nodeHeader;
    EntryNum entryNum = 0;

    rc = goToLeaf(ixfileHandle, attribute, key, entryNum, nodeHeader, pageSpace);
    if (rc != SUCCESS) {
        free (pageSpace);
        return rc;
    } 

    // check if leaf page has key/rid
    if (searchLeafForKeyAndRid(pageSpace, attribute, key, rid, entryNum) == false) {
        free (pageSpace);
        return IX_ENTRY_DN_EXIST;
    }
    // check if entry is already deleted
    EntrySlot entrySlot;
    getEntrySlot(pageSpace, entryNum, entrySlot);
    if (entrySlot.isDeleted == true) {
        free (pageSpace);
        return IX_ENTRY_DN_EXIST;
    }
     
    // delete entry
    entrySlot.isDeleted = true;
    setEntrySlot(entryNum, entrySlot, pageSpace);
    ixfileHandle.writePage(nodeHeader.pageNum, pageSpace); 
    
    free (pageSpace);
    return SUCCESS;
}


RC IndexManager::scan(IXFileHandle &ixfileHandle,
        const Attribute &attribute,
        const void      *lowKey,
        const void      *highKey,
        bool	        lowKeyInclusive,
        bool        	highKeyInclusive,
        IX_ScanIterator &ix_ScanIterator)
{
    return ix_ScanIterator.scanInit(ixfileHandle, attribute, lowKey, highKey, lowKeyInclusive, highKeyInclusive);
}

IX_ScanIterator::IX_ScanIterator()
: _pageSpace(NULL), _entryNum(0), _lowKey(NULL), _highKey(NULL), _lowKeyInclusive(false), _highKeyInclusive(false)
{
    // Initialize the internal IndexManager instanc
    _ixm = IndexManager::instance();
}

IX_ScanIterator::~IX_ScanIterator()
{
    if (_pageSpace != NULL) {
        free (_pageSpace);
        _pageSpace = NULL;
    }
    if (_lowKey != NULL) {
        free (_lowKey);
        _lowKey = NULL;
    }
    if (_highKey != NULL) {
        free (_highKey);
        _highKey = NULL;
    }
 }

RC IX_ScanIterator::findFirstEntry() {
    cerr << ">>> findFirstEntry" << endl;
    // find start with lowKey
    RC rc = SUCCESS;
    rc = _ixfh->readPage(_ixfh->getRoot(), _pageSpace);
    if (rc != SUCCESS) {
        return rc;
    }
    _ixm->getNodeHeader(_pageSpace, _nodeHeader);     
    _entryNum = 0;
    rc = _ixm->goToLeaf(*_ixfh, _attribute, _lowKey, _entryNum, _nodeHeader, _pageSpace);
    if (rc != SUCCESS) {
        return rc;
    }
    assert(_nodeHeader.nodeType == LEAF && "scanInit should be to a leaf"); 
    _entryNum = 0;
    if (_lowKey != NULL) {
        if(_lowKeyInclusive) {
            _ixm->searchLeafForKeyInclusive(_pageSpace, _attribute, _lowKey, _entryNum);
        } else {
            _ixm->searchLeafForKeyExclusive(_pageSpace, _attribute, _lowKey, _entryNum);
        }
    }
    _searched = true; 
    cerr << "<<< findFirstEntry" << endl;
    return SUCCESS;
}

 // WARNING: pointers to lowKey and HighKey -- possible dangling
RC IX_ScanIterator::scanInit(IXFileHandle     &ixfh,
            const Attribute  &attribute,
            const void       *lowKey,
            const void       *highKey,
            bool	     lowKeyInclusive,
            bool             highKeyInclusive) 
{
    cerr << ">>> scanInit" << endl;
    _ixfh = &ixfh;
    _attribute.name = attribute.name;
    _attribute.type = attribute.type;
    _attribute.length = attribute.length;
    
    EntrySlot entrySlot; 
    _ixm->setEntrySlotLengthToKey(attribute, lowKey, entrySlot);
    _lowKey = (char*)calloc(entrySlot.length, 1);
    memcpy(_lowKey, (char*)lowKey, entrySlot.length);

    _ixm->setEntrySlotLengthToKey(attribute, highKey, entrySlot);
    _highKey = (char*)calloc(entrySlot.length, 1);
    memcpy(_highKey, (char*)highKey, entrySlot.length);

    _lowKeyInclusive = lowKeyInclusive;
    _highKeyInclusive = highKeyInclusive;
    _searched = false; 
   
    _pageSpace = calloc(PAGE_SIZE, 1);
    if (_pageSpace == NULL) {
        return IX_MALLOC_FAILED;
    }
    // check _ixfh with read
    if (_ixfh->readPage(0, _pageSpace)) {
        return IX_READ_FAILED;
    } 

    cerr << "<<< scanInit: HighKey = ";
    _ixm->printKey(_attribute, _highKey);
    cerr << endl;
    return SUCCESS;
}
 
RC IX_ScanIterator::getNextEntry(RID &rid, void *key)
{
    cerr << ">>> getNextEntry: Page#" << _nodeHeader.pageNum << ", RSib#" << _nodeHeader.rightSibling << endl;
    cerr << "                : _highKey = ";
    _ixm->printKey(_attribute, _highKey);
    cerr << endl;
    RC rc = SUCCESS;
    EntrySlot entrySlot;
    if (!_searched) {
        findFirstEntry();
    }
    for (;;) {
        if (_entryNum < _nodeHeader.entryCount) {
            // get slot
	    _ixm->getEntrySlot(_pageSpace, _entryNum, entrySlot);
            // check if deleted
            if (entrySlot.isDeleted == true) {
                ++_entryNum;
                continue;
            } 
	    // get key
	    memcpy(key, (char*)_pageSpace + entrySlot.offset + sizeof(RID), entrySlot.length - sizeof(RID));
           
            // If highKey is null, return result
	    if (_highKey == NULL) {
		// get rid
                memcpy(&rid, (char*)_pageSpace + entrySlot.offset, sizeof(RID)); 
		++_entryNum;

                // DELETE BELOW
                cerr << "<<< INF: getNextEntry: [";
                _ixm->printKey(_attribute, key);
                cerr << ",";
                _ixm->printRid(rid);
                cerr << "]" << endl;
                // DELETE ABOVE

 		return SUCCESS;
            } else {
                // Compare with HighKey
                int result = _ixm->compareKeys (_attribute, key, _highKey);
                cerr << "compare: (";
                _ixm->printKey(_attribute, key);
                cerr << ", ";
                _ixm->printKey(_attribute, _highKey);
                cerr << ") result = " << result << endl;
                if (result > 0 || (result == 0 && !_highKeyInclusive)) {
                    cerr << "<<< EOF: GT or EXEQ" << endl;
                    return IX_EOF;
                } 
 
                // Case 1: return if result is less than HighKey or if result is same and inclusive
		if (result < 0) { 
                    memcpy(&rid, (char*)_pageSpace + entrySlot.offset, sizeof(RID)); 
                    ++_entryNum;

                    // DELETE BELOW
                    cerr << "<<< LT: getNextEntry: [";
                    _ixm->printKey(_attribute, key);
                    cerr << ",";
                    _ixm->printRid(rid);
                    cerr << "]" << endl;
                    // DELETE ABOVE

                    return SUCCESS;
		}
                if (_highKeyInclusive && result == 0) {
                    memcpy(&rid, (char*)_pageSpace + entrySlot.offset, sizeof(RID)); 
                    ++_entryNum;

                    // DELETE BELOW
                    cerr << "<<< EQ: getNextEntry: [";
                    _ixm->printKey(_attribute, key);
                    cerr << ",";
                    _ixm->printRid(rid);
                    cerr << "]" << endl;
                    // DELETE ABOVE

                    return SUCCESS;
                }	                  
                // else DONE
                cerr << "EX or GT: RETURNED HIGH IX_EOF" << endl;
                return IX_EOF; 
	    } 
        } else {
            //get next page
            if (_nodeHeader.rightSibling == 0) {
                // DELETE BELOW
                cerr << "<<< getNextEntry: EOF";
                // DELETE ABOVE

                return IX_EOF;
            }
            rc = _ixfh->readPage(_nodeHeader.rightSibling, _pageSpace);
            if (rc != SUCCESS) {
                return rc;
            }
            _ixm->getNodeHeader(_pageSpace, _nodeHeader);
            _entryNum = 0;
        }
    }

    assert(false && "should not be here");
}

RC IX_ScanIterator::close()
{
    if (_pageSpace != NULL) {
        free (_pageSpace);
        _pageSpace = NULL;
    }
    if (_lowKey != NULL) {
        free (_lowKey);
        _lowKey = NULL;
    }
    if (_highKey != NULL) {
        free (_highKey);
        _highKey = NULL;
    }
      return SUCCESS;
}


IXFileHandle::IXFileHandle()
{
    ixReadPageCounter = 0;
    ixWritePageCounter = 0;
    ixAppendPageCounter = 0;
}

IXFileHandle::~IXFileHandle()
{
}

bool IXFileHandle::hasOpenFile() {
   return false; 
}

inline PageNum IXFileHandle::getRoot() {
    return _root;
}

inline void IXFileHandle::setRoot(PageNum newRoot) {
    _root = newRoot;
}

unsigned IXFileHandle::getNumberOfPages() {
    return _fh.getNumberOfPages();
}

RC IXFileHandle::collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount)
{
    readPageCount = ixReadPageCounter;
    writePageCount = ixWritePageCounter;
    appendPageCount = ixAppendPageCounter;

    return SUCCESS;
}

RC IXFileHandle::readPage( PageNum pagenum, void *data )
{
    ++ixReadPageCounter;

    return _fh.readPage(pagenum, data);
}

RC IXFileHandle::writePage( PageNum pagenum, void *data )
{
    ++ixWritePageCounter;

    return _fh.writePage(pagenum, data);
}

RC IXFileHandle::appendPage( void *data )
{
    ++ixAppendPageCounter;

    return _fh.appendPage(data);
}

void IndexManager::printBtree(IXFileHandle &ixfileHandle, const Attribute &attribute) {
    cout << "{";
    unsigned depth = 0;
    printBtree2(ixfileHandle, ixfileHandle.getRoot(), attribute, depth);
    cout << endl << "}" << endl;
}

void IndexManager::printDepth(const unsigned depth) const {
    for (unsigned i = 0; i < depth; ++i) {
        cout << "    ";
    }
}

inline void IndexManager::getRid(const void* pageSpace, const EntrySlot &entrySlot, RID &rid) {
    memcpy(&rid, (char*) pageSpace + entrySlot.offset, sizeof(RID));
}
   
void IndexManager::printRid(const RID &rid) {
    cout << "(" << rid.pageNum << "," << rid.slotNum << ")";
}

void IndexManager::printKey(const Attribute &attribute, const void* key) {
    if (key == NULL) {
        cout << "NULL";
        return;
    }
    if (attribute.type == TypeReal) {
        float floatValue = 0.0;
        memcpy (&floatValue, key, sizeof(float));
        cout << floatValue;
    } else {
        int32_t intValue = 0; 
        memcpy(&intValue, key, sizeof(int32_t));
        if (attribute.type == TypeInt) {
            cout << intValue;
        } else if (attribute.type == TypeVarChar) {
            char* varChar = (char*) calloc(intValue + 1, sizeof(char));
            varChar[intValue] = '\0';                
            memcpy (varChar, (char*)key + sizeof(int32_t), intValue);
            fprintf (stdout, "%s", varChar);
            free (varChar);
        } else {
            assert(false && "Error: Invalid attribute type\n");
        }
 
   }
}

void IndexManager::printLeafEntries(const Attribute &attribute, const void* pageSpace, const NodeHeader &nodeHeader) {
    EntrySlot entrySlot;
    RID rid;
    Offset offset = 0;
    Offset nextOffset = 0;
    unsigned i = 0;
    if (nodeHeader.entryCount > 0) {
        // Print first uniquekey + rids:
        // print uniquekey header
        cout << "\"" << std::flush;
        getEntrySlot(pageSpace, i, entrySlot);
        offset = entrySlot.offset + sizeof(RID);
        printKey(attribute, (char*)pageSpace + entrySlot.offset + sizeof(RID)); 
        cout << ": [" << std::flush;
        memcpy(&rid, (char*)pageSpace + entrySlot.offset, sizeof(RID));
        printRid(rid); 
        // print rids while key is same
        ++i;
        for (;i < nodeHeader.entryCount; ++i) {
            getEntrySlot(pageSpace, i, entrySlot);
            nextOffset = entrySlot.offset + sizeof(RID);
            if (compareKeys(attribute, (char*)pageSpace + offset, (char*)pageSpace + nextOffset) == true) {
                memcpy(&rid, (char*)pageSpace + entrySlot.offset, sizeof(RID));
                printRid(rid); 
            } else {
                break;
            }
        }
        // print uniquekey footer
        cout << "]\"";
        while (i < nodeHeader.entryCount) {
            cout << ",";
            // print uniquekey header
            cout << "\"";
            getEntrySlot(pageSpace, i, entrySlot);
            offset = entrySlot.offset + sizeof(RID);
            printKey(attribute, (char*)pageSpace + entrySlot.offset + sizeof(RID)); 
            cout << ": [";
            memcpy(&rid, (char*)pageSpace + entrySlot.offset, sizeof(RID));
            printRid(rid); 
            // print rids while key is same
            ++i;
            for (;i < nodeHeader.entryCount; ++i) {
                getEntrySlot(pageSpace, i, entrySlot);
                nextOffset = entrySlot.offset + sizeof(RID);
                if (compareKeys(attribute, (char*)pageSpace + offset, (char*)pageSpace + nextOffset) == true) {
                    memcpy(&rid, (char*)pageSpace + entrySlot.offset, sizeof(RID));
                    printRid(rid); 
                } else {
                    break;
                }
            }
            // print uniquekey footer
            cout << "]\"";
         }
//BOOKMARK: OK BELOW 
    }
}

void IndexManager::printInnerKeys(const Attribute &attribute, const void* pageSpace, const NodeHeader &nodeHeader) {
    if (nodeHeader.entryCount > 0) { 
        cout << "\"";
        EntrySlot entrySlot;
        getEntrySlot(pageSpace, 0, entrySlot);
        printKey(attribute, (char*)pageSpace + entrySlot.offset);
        cout << "\"";
        for (unsigned i = 1; i < nodeHeader.entryCount; ++i) {
            cout << ",\"";
            getEntrySlot(pageSpace, i, entrySlot);
            printKey(attribute, (char*)pageSpace + entrySlot.offset);
            cout << "\"";
        }
    }
}

void IndexManager::printBtree2(IXFileHandle &ixfh, const PageNum node, const Attribute &attribute, const unsigned depth) {
    RC rc = SUCCESS;
    void *pageSpace = (char*)calloc(PAGE_SIZE, 1);    
    rc = ixfh.readPage(node, pageSpace);
    if (rc != SUCCESS) {
        free (pageSpace);
        return;
    }
    NodeHeader nodeHeader;
    getNodeHeader(pageSpace, nodeHeader);
    printDepth(depth);
    // 0. print out node header
    if (nodeHeader.pageNum != ixfh.getRoot()) {
        cout << "{";
    } 
    // 1. print out node
    // A1. print out key header 
    cout << "\"keys\":[";
    // A2. print out entries and commas
    if (nodeHeader.nodeType == LEAF) {
        printLeafEntries(attribute, pageSpace, nodeHeader);
    } else {
        printInnerKeys(attribute, pageSpace, nodeHeader);
    } 
    cout << std::flush;
    // A3. print out key footer
    cout << "]";
    // B. print out child or leaf end
    if (nodeHeader.nodeType == LEAF) {
        // B0. printout leaf end
        if (nodeHeader.pageNum != ixfh.getRoot()) {
            cout << "}";
        }
        free (pageSpace);
        return;
    } else {
        cout << "," << endl;
        printDepth(depth);
        // B1. print out children header
        if (nodeHeader.pageNum != ixfh.getRoot()) {
            cout << " "; // accounts for "{"
        }
        cout << "\"children\":[" << endl;
        // B2. recursively print each child:
        if (nodeHeader.entryCount > 0) {
            printBtree2(ixfh, nodeHeader.firstChild, attribute, depth + 1);
            EntrySlot entrySlot;
            for (unsigned i = 0; i < nodeHeader.entryCount; ++i) {
                cout << "," << endl;
                getEntrySlot(pageSpace, i, entrySlot);
                printBtree2(ixfh, entrySlot.rightChild, attribute, depth + 1);
            }
        }
        // B3. print out children footer
        printDepth(depth);
        cout << endl << "]";
    }
    // 2. print out node footer
    if (nodeHeader.pageNum != ixfh.getRoot()) {
        cout << "}";  
    }
    free (pageSpace);
    return;
}
