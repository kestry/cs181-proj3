#ifndef _ix_h_
#define _ix_h_

#include <vector>
#include <string>

#include "../rbf/rbfm.h"

#define IX_EOF (-1)  // end of the index scan

#define IX_IXFH_FAILED    40
#define IX_CREATE_FAILED  41
#define IX_MALLOC_FAILED  42
#define IX_OPEN_FAILED    43
#define IX_APPEND_FAILED  44
#define IX_READ_FAILED    45
#define IX_WRITE_FAILED   46
#define IX_ENTRY_DN_EXIST 47
#define IX_READ_AFTER_DEL 48
#define IX_DUP_INSERT     49 
#define IX_SPLIT_SAME_KEY 50 
#define IX_LARGE_KEY      51 

/* stuff in rbfm:

typedef enum { TypeInt, TypeReal, TypeVarChar } AttrType;

typedef unsigned AttrLength;

struct Attribute {
    string   name;     // attribute name
    AttrType type;     // attribute type
    AttrLength length; // attribute length

    Attribute() {
        length = 0;
    }
} Attribute;

// typedef uint32_t PageNum // modified pfm.h from unsigned

*/
typedef uint32_t EntryNum;

typedef char NodeType;
const NodeType LEAF = 0;
const NodeType INNER = 1; 

typedef uint32_t Count;
typedef uint32_t Length;
typedef int32_t Offset;
// void* key format:
// INT & REAL: [ value  ]
// VARCHAR:    [ length | value ]

typedef struct MetaHeader {
    PageNum root;

    MetaHeader() {
        root = 1;
    }
} MetaHeader;

typedef struct NodeHeader {
    // Node Info
    PageNum  pageNum;
    NodeType nodeType;

    // Content Info  
    Count entryCount;
    Offset freeSpaceOffset;
    Length freeSpaceSize;

    // Family Info
    PageNum leftSibling; //only for leaf
    PageNum rightSibling; // only for leaf
    PageNum firstChild; // only for inner

    // Constructor
    NodeHeader() {
        pageNum = 0; 
        nodeType = LEAF;
       
        // parent init by default
	leftSibling = 0;
	rightSibling = 0;
        firstChild = 0;

        entryCount = 0;
	freeSpaceOffset = PAGE_SIZE;
	freeSpaceSize = PAGE_SIZE - sizeof(NodeHeader);
    }

    void clear() {
        pageNum = 0; 
        nodeType = LEAF;
       
        // parent init by default
	leftSibling = 0;
	rightSibling = 0;
        firstChild = 0;

        entryCount = 0;
	freeSpaceOffset = PAGE_SIZE;
	freeSpaceSize = PAGE_SIZE - sizeof(NodeHeader);
      }
        	
} NodeHeader;

// Negative offset indicates deletedEntry
typedef struct EntrySlot {
   Length length;
   Offset offset;
   PageNum rightChild;
   bool isDeleted;
   
   // Constructor 
   EntrySlot() {
      length = 0;
      offset = 0;
      rightChild = 0;
      isDeleted = false;
   }
} EntrySlot; 

class IXFileHandle {
    public:
    // variables to keep counter for each operation
    unsigned ixReadPageCounter;
    unsigned ixWritePageCounter;
    unsigned ixAppendPageCounter;

    // Constructor
    IXFileHandle();

    // Destructor
    ~IXFileHandle();

	// Put the current counter values of associated PF FileHandles into variables
	RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount);

    PageNum getRoot();
    void setRoot(PageNum newRoot);
    unsigned getNumberOfPages();
    RC readPage( PageNum pagenum, void *data );
    RC writePage( PageNum pagenum, void *data );
    RC appendPage( void *data );
    
    bool hasOpenFile();
  
    friend class IndexManager;
    
private:
    FileHandle _fh;
    PageNum _root;
 
};

class IX_ScanIterator;

class IndexManager {

    public:
        static IndexManager* instance();

        // Create an index file.
        RC createFile(const string &fileName);

        // Delete an index file.
        RC destroyFile(const string &fileName);

        // Open an index and return an ixfileHandle.
        RC openFile(const string &fileName, IXFileHandle &ixfileHandle);

        // Close an ixfileHandle for an index.
        RC closeFile(IXFileHandle &ixfileHandle);

        // Insert an entry into the given index that is indicated by the given ixfileHandle.
        RC insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        // Delete an entry from the given index that is indicated by the given ixfileHandle.
        RC deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        // Initialize and IX_ScanIterator to support a range search
        RC scan(IXFileHandle &ixfileHandle,
                const Attribute &attribute,
                const void *lowKey,
                const void *highKey,
                bool lowKeyInclusive,
                bool highKeyInclusive,
                IX_ScanIterator &ix_ScanIterator);

        // Print the B+ tree in pre-order (in a JSON record format)
        void printBtree(IXFileHandle &ixfileHandle, const Attribute &attribute);

        RC goToLeaf(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, EntryNum &entryNum, NodeHeader &nodeHeader, void* &pageSpace);

        friend class IX_ScanIterator;

    protected:
        IndexManager();
        ~IndexManager();

    private:
    public:
        static IndexManager *_index_manager;
        static PagedFileManager *_pfm;

        void getNodeHeader(const void* pageSpace, NodeHeader &nodeHeader);
        void setNodeHeader(const NodeHeader &nodeHeader, void* &pageSpace);
        void getEntrySlot(const void* pageSpace, EntryNum entryNum, EntrySlot &entrySlot);
        void setEntrySlot(EntryNum entryNum, const EntrySlot &entrySlot, void* &pageSpace);

        void setEntrySlotLengthToKey(const Attribute &attribute, const void *key, EntrySlot &entrySlot);
        int  compareKeys(const Attribute &attribute, const void* key1, const void* key2);
        bool isEqual(RID rid1, RID rid2);

        bool binarySearchNodeForKey(const void *pageSpace, const Attribute &attribute, const void *key, EntryNum &entryNum);
        bool searchLeafForKeyInclusive(const void *pageSpace, const Attribute &attribute, const void *key, EntryNum &entryNum);
        void searchLeafForKeyExclusive(const void *pageSpace, const Attribute &attribute, const void *key, EntryNum &entryNum);
        bool searchLeafForKeyAndRid(const void *pageSpace, const Attribute &attribute, const void *key, const RID &rid, EntryNum &entryNum);


        void insertIntoPosition(const Attribute &attribute, const void* entry, const EntryNum &entryNum, EntrySlot &entrySlot, NodeHeader &nodeHeader, void* &pageSpace);
        void insertIntoLeafPosition(const Attribute &attribute, const void* key, const RID &rid, const EntryNum &entryNum, EntrySlot &entrySlot, NodeHeader &nodeHeader, void* &pageSpace);
        void insertIntoInnerPosition(const Attribute &attribute, const void* key, const EntryNum &entryNum, EntrySlot &entrySlot, NodeHeader &nodeHeader, void* &pageSpace);

        RC setMetaHeader(IXFileHandle &ixfh, MetaHeader &metaHeader);
        RC setRoot(IXFileHandle &ixfh, PageNum newRoot);

        void copy(const Attribute &attribute, const NodeHeader &nodeHeader, const void* pageSpace, unsigned start, unsigned end, NodeHeader &newNodeHeader, void* &newPageSpace);


        RC splitRoot(IXFileHandle &ixfh, const Attribute &attribute, const PageNum leftChild, EntrySlot &entrySlot, void* &key);

        RC splitInsertIntoLeafPosition(IXFileHandle &ixfh, const Attribute &attribute, const void* key, const RID &rid, EntryNum entryNum, EntrySlot &toParentEntrySlot, void* &toParentEntry, NodeHeader &nodeHeader, void* &pageSpace); 

        RC splitInsertIntoInnerPosition(IXFileHandle &ixfh, const Attribute &attribute, EntryNum entryNum, EntrySlot &fromToParentEntrySlot, void* &fromToParentEntry, NodeHeader &nodeHeader, void* &pageSpace); 

        RC insertEntry2 (IXFileHandle &ixfh, const Attribute &attribute, const void *key, const RID &rid, PageNum node, EntrySlot &newChildEntrySlot, void* &newChildEntry);




        void printDepth(const unsigned depth) const;
        void getRid(const void* pageSpace, const EntrySlot &entrySlot, RID &rid);
        void printRid(const RID &rid);
        void printKey(const Attribute &attribute, const void* key);
        void printLeafEntries(const Attribute &attribute, const void* pageSpace, const NodeHeader &nodeHeader);
        void printInnerKeys(const Attribute &attribute, const void* pageSpace, const NodeHeader &nodeHeader);
        void printBtree2(IXFileHandle &ixfh, const PageNum node, const Attribute &attribute, const unsigned depth);
};

class IX_ScanIterator {
    public:
        // Constructor
        IX_ScanIterator();

        // Destructor
        ~IX_ScanIterator();

        // Get next matching entry
        RC getNextEntry(RID &rid, void *key);

        // Terminate index scan
        RC close();

        friend class IndexManager;

    private:
        static IndexManager *_ixm;
        void *_pageSpace;

        NodeHeader       _nodeHeader;
        EntryNum         _entryNum;
      
        IXFileHandle*    _ixfh;
        Attribute        _attribute;

        void            *_lowKey;
        void            *_highKey;
        bool             _lowKeyInclusive;
        bool             _highKeyInclusive;
        bool             _searched;

        RC scanInit(IXFileHandle &ixfh,
            const Attribute      &attribute,
            const void           *lowKey,
            const void           *highKey,
            bool	         lowKeyInclusive,
            bool        	 highKeyInclusive);
        RC findFirstEntry();
};



#endif
